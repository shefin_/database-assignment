SELECT hall_id, day as "no movie day", show_num FROM show 
	WHERE movie_name is null
		ORDER BY hall_id;