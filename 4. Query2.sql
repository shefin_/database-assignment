SELECT cinema.id as cinema_id, cinema.name as cinema_name, city_id, COUNT(movie_name) as movie_released_count from 
cinema INNER JOIN 
(SELECT DISTINCT movie_name, cinema_id FROM
	hall INNER JOIN "show"
		ON hall.id = "show".hall_id
			WHERE show.movie_name is not null and
			show.day BETWEEN '2022-01-01' and '2022-12-31') as t1 
				ON cinema.id = t1.cinema_id
					GROUP BY cinema.id ORDER BY cinema_id;