INSERT INTO city VALUES(1, 'Dhaka');
INSERT INTO city VALUES(2, 'Chittagong');

INSERT INTO cinema VALUES(1, 1, 'Progoti');
INSERT INTO cinema VALUES(2, 1, 'Star-Cinema');
INSERT INTO cinema VALUES(3, 2, 'Jagoron');
INSERT INTO cinema VALUES(4, 2, 'Cineplex');

INSERT INTO "user" VALUES(1, 'Kabir', 1);
INSERT INTO "user" VALUES(2, 'Jakir', 1);
INSERT INTO "user" VALUES(3, 'Rakib', 2);
INSERT INTO "user" VALUES(4, 'Baker', 2);

INSERT INTO hall(id, total_seat, weekend_id, cinema_id) VALUES(1, 5, 1, 1);
INSERT INTO hall VALUES(2, 10, 2, 1);
INSERT INTO hall VALUES(3, 20, 7, 1);
INSERT INTO hall VALUES(4, 50, 1, 2);
INSERT INTO hall VALUES(5, 7, 3, 2);
INSERT INTO hall VALUES(6, 4, 5, 2);
INSERT INTO hall VALUES(7, 20, 4, 3);
INSERT INTO hall VALUES(8, 15, 1, 3);
INSERT INTO hall VALUES(9, 25, 2, 3);
INSERT INTO hall VALUES(10, 50, 6, 4);
INSERT INTO hall VALUES(11, 3, 1, 4);
INSERT INTO hall VALUES(12, 30, 5, 4);

INSERT INTO show(id, movie_name, day, show_num, ticket_price, hall_id, booked_seat)
		 VALUES(1, '3 Idiots', '2022-01-15', 1, 300, 1, 0);
INSERT INTO show VALUES(2, '3 Idiots', '2022-01-15', 2, 400, 1, 0);
INSERT INTO show VALUES(3, 'Interstellar', '2022-12-20', 1, 500, 2, 0);
INSERT INTO show VALUES(4, 'Interstellar', '2022-12-21', 2, 400, 2, 0);
INSERT INTO show VALUES(5, 'Martian', '2022-12-13', 2, 1000, 3, 0);
INSERT INTO show VALUES(6, 'Shutter Island', '2022-11-13', 1, 1000, 4, 0);
INSERT INTO show VALUES(7, 'Prestige', '2021-10-21', 2, 700, 5, 0);
INSERT INTO show(id, day, show_num, hall_id)
		VALUES(8, '2021-10-12', 1, 1);
INSERT INTO show(id, day, show_num, hall_id)
		VALUES(9, '2021-10-12', 1, 6);
INSERT INTO show VALUES(10, 'Nemesis', '2021-7-12', 1, 500, 7, 0);
INSERT INTO show VALUES(11, 'Looper', '2021-4-01', 2, 300, 8, 0);