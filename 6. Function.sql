CREATE OR REPLACE FUNCTION getWeekend(hall_id integer)
RETURNS integer as $weekend$
declare
	week_id integer;
BEGIN
	SELECT weekend_id into week_id FROM hall WHERE hall.id=hall_id;
	RETURN week_id;
END;
$weekend$ LANGUAGE plpgsql;


SELECT getWeekend(3) as "Weekend Id";