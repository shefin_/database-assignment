SELECT cinema.id as cinema_id, cinema.name as cinema_name, city_id, movie_name from 
cinema INNER JOIN 
(SELECT DISTINCT movie_name, cinema_id FROM
	hall INNER JOIN "show"
		ON hall.id = "show".hall_id
			WHERE show.movie_name is not null
				ORDER BY cinema_id) as t1 
					ON cinema.id = t1.cinema_id;