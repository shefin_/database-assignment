CREATE TABLE "city" (
  "id" int,
  "name" varchar,
  PRIMARY KEY ("id")
);

CREATE TABLE "cinema" (
  "id" int,
  "city_id" int,
  "name" varchar,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("city_id") REFERENCES "city" ("id")
);

CREATE TABLE "user" (
  "id" int,
  "name" varchar,
  "city_id" int,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("city_id") REFERENCES "city" ("id")
);

CREATE TABLE "hall" (
  "id" int,
  "total_seat" int,
  "weekend_id" int,
  "cinema_id" int,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("cinema_id") REFERENCES "cinema" ("id")
);

CREATE TABLE "show" (
  "id" int,
  "movie_name" varchar,
  "day" date,
  "show_num" int,
  "ticket_price" real DEFAULT 0,
  "hall_id" int,
  "booked_seat" int DEFAULT 0,
  PRIMARY KEY ("id"),
  FOREIGN KEY ("hall_id") REFERENCES "hall" ("id")
);

CREATE TABLE "usershowrelation" (
  "user_id" int,
  "show_id" int,
  "ticket_count" int,
  PRIMARY KEY ("user_id", "show_id"),
  FOREIGN KEY ("user_id") REFERENCES "user" ("id"),
  FOREIGN KEY ("show_id") REFERENCES "show" ("id")
);