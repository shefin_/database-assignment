CREATE OR REPLACE PROCEDURE book_ticket(userId integer, ticketCount integer, showId integer,
									   INOUT returnVal integer DEFAULT null)
AS $$
DECLARE
	total integer;
	booked integer;
	h_id integer;
	price integer;
	own_ticket integer;
BEGIN
	SELECT hall_id, booked_seat, ticket_price INTO h_id, booked, price 
		FROM show WHERE id=showId;
	SELECT total_seat INTO total FROM hall WHERE id=h_id;
	
	IF booked IS NULL OR booked+ticketCount > total THEN
		returnVal = -1;
		RETURN;
	END IF;
	
	SELECT usershowrelation.ticket_count INTO own_ticket FROM usershowrelation 
		WHERE usershowrelation.user_id = userId AND 
			  usershowrelation.show_id = showId;
		
	if (own_ticket IS NULL AND ticketCount > 4) OR (own_ticket+ticketCount>4) THEN
		returnVal = -2;
		RETURN;
	END IF;
	
	IF own_ticket IS NULL THEN
		INSERT INTO usershowrelation VALUES(userId, showId, ticketCount);
	ELSE
		UPDATE usershowrelation SET ticket_count=own_ticket+ticketCount
			WHERE usershowrelation.user_id = userId AND 
			  usershowrelation.show_id = showId;
	END IF;
	
	UPDATE show SET booked_seat=booked_seat+ticketCount WHERE id=showId;
	returnVal = price*ticketCount;
	
END;
$$ language plpgsql;